module.exports = function(grunt){

    grunt.initConfig({

      sass: {
        dist: {
          options: {
            style: 'compressed'
          },
          files: {
            'src/css/style-min.css': 'src/sass/style-min.scss'
          }
        }
      },
      watch: {
        css: {
          files: 'src/sass/**/*.scss',
          tasks: ['sass'],
          options: {
            livereload: true,
          },
        },
      },
  		browserSync: {
        public: {
          bsFiles: {
            src : ['src/**/*']
          },
          options: {
              watchTask: true,
              server: {
                  baseDir: "src"
              }
          }
        }
      }

    });

    grunt.registerTask('dev', ['sass']);
    grunt.registerTask('server', ['browserSync', 'watch']);
    grunt.registerTask('default', ['dev','server']);

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');
}
