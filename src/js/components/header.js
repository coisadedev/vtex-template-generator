
//CRIANDO O HTML DE CADA COMPONENTE
var logo 	  = '<div class="header-logo"><a href="/"><img src="images/logo-benevento-v2.png"  alt="" /></a></div>';
var busca 	  = '<div class="header-busca"><vtex.cmc:fullTextSearchBox/></div>';
var iconesCpc = '<div class="header-icones"><ul><li><a href="/account"><i class="fa fa-user" aria-hidden="true"></i></a></li><li><a href="/account/orders"><i class="fa fa-list" aria-hidden="true"></i></a></li><li><a href="/checkout"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li></ul></div>';

//VERIFICANDO QUAL COMPONENTE O USUARIO 
//CLICO E COLOCANDO ELE NO HEADER
$('.drop-box label').click(function(event) {
var idComponente = $(this).attr('id');         
  
	if( idComponente == 'logo'){
		$('header .container-center').append(logo);
	}
  	if( idComponente == 'busca'){
    	$('header .container-center').append(busca);
  	}
  	if( idComponente == 'icones-cpc'){
    	$('header .container-center').append(iconesCpc);
  	}

});

//ABRE DIV EDITA LOGO E ESCONDE A DA BUSCA E ICONES
$(document).on('click', '.header-logo', function(event) {
	event.preventDefault();
 	$('.modal-edite-busca').hide();
 	$('.modal-edite-icones').hide();
  	$('.modal-edite-logo').show();    
});
//ABRE DIV EDITA BUSCA E ESCONDE A DA LOGO E ICONES
$(document).on('click', '.header-busca', function(event) {
	event.preventDefault();
  	$('.modal-edite-logo').hide();
  	$('.modal-edite-icones').hide();
  	$('.modal-edite-busca').show();
});
//ABRE DIV ICONES LOGO E ESCONDE A DA BUSCA E LOGO
$(document).on('click', '.header-icones', function(event) {
	event.preventDefault();
  	$('.modal-edite-logo').hide();    
  	$('.modal-edite-busca').hide();    
  	$('.modal-edite-icones').show();
});

//FECHA O MODAL DE EDITAR OS COMPONENTES
$('.modal-edite-logo .fechar, .modal-edite-busca .fechar, .modal-edite-icones .fechar').on('click', function(event) {
	event.preventDefault();
	$('.modal').hide();
});

//EDITANDO POSIÇÃO E SRC DO LOGO
$('#edite-logo').on('click', function(event) {
	event.preventDefault();

	//PEGANDO OS VALORES DOS CAMPOS SELECIONADO PELO USUARIO
	var valorAlinhamentoHorizontal = $('.modal-edite-logo #select-alinhamento-horizontal').val();
	var valorAlinhamentoVertical = $('.modal-edite-logo #select-alinhamento-vertical').val();
	var valorUrl = $('.modal-edite-logo #url-image').val();

		//VERIFICANDO SE JÁ EXISTE UM CLASSE DE POSICIONAMENTO HORIZONTAL
		//SE EXISTIR RETIRAR PARA COLOCAR A NOVA ESCOLHIDA PELO USUARIO
		$('header').each(function(index, el) {
  			if ( $('.header-logo').hasClass('header-h-left') ) {
    			$('.header-logo').removeClass('header-h-left');      
  			}else if ( $('.header-logo').hasClass('header-h-center') ) {
    			$('.header-logo').removeClass('header-h-center');      
  			}else if ( $('.header-logo').hasClass('header-h-right') ) {
    			$('.header-logo').removeClass('header-h-right');
  			}
		});
		//VERIFICANDO SE JÁ EXISTE UM CLASSE DE POSICIONAMENTO VERTICAL
		//SE EXISTIR RETIRAR PARA COLOCAR A NOVA ESCOLHIDA PELO USUARIO
		$('header').each(function(index, el) {
		  if ( $('.header-logo').hasClass('header-v-top') ) {
		    $('.header-logo').removeClass('header-v-top');
		  }else if ( $('.header-logo').hasClass('header-v-center') ) {
		    $('.header-logo').removeClass('header-v-center');
		  }else if ( $('.header-logo').hasClass('header-v-bottom') ) {
		    $('.header-logo').removeClass('header-v-bottom');
		  }      
		});

			//COLOCANDO AS CLASS DE POSICIONAMENTO E A URL DO SRC
			$('.header-logo').addClass(valorAlinhamentoHorizontal);
			$('.header-logo').addClass(valorAlinhamentoVertical);
			$('.header-logo img').attr('src', valorUrl);

});

//EDITANDO POSIÇÃO DO CAMPO DE BUSCA
$('#edite-busca').on('click', function(event) {
	event.preventDefault();

	//PEGANDO OS VALORES DOS CAMPOS SELECIONADO PELO USUARIO
	var buscavalorAlinhamentoHorizontal = $('.modal-edite-busca #select-alinhamento-horizontal').val();
	var buscavalorAlinhamentoVertical = $('.modal-edite-busca #select-alinhamento-vertical').val();

		//VERIFICANDO SE JÁ EXISTE UM CLASSE DE POSICIONAMENTO HORIZONTAL
		//SE EXISTIR RETIRAR PARA COLOCAR A NOVA ESCOLHIDA PELO USUARIO
		$('header').each(function(index, el) {
			if ( $('.header-busca').hasClass('header-busca-h-left') ) {
		    	$('.header-busca').removeClass('header-busca-h-left');      
		  	}else if ( $('.header-busca').hasClass('header-busca-h-center') ) {
		    	$('.header-busca').removeClass('header-busca-h-center');      
		  	}else if ( $('.header-busca').hasClass('header-busca-h-right') ) {
		    	$('.header-busca').removeClass('header-busca-h-right');
		  	}
		});
		//VERIFICANDO SE JÁ EXISTE UM CLASSE DE POSICIONAMENTO VERTICAL
		//SE EXISTIR RETIRAR PARA COLOCAR A NOVA ESCOLHIDA PELO USUARIO
		$('header').each(function(index, el) {
		  	if ( $('.header-busca').hasClass('header-busca-v-top') ) {
		    	$('.header-busca').removeClass('header-busca-v-top');
		  	}else if ( $('.header-busca').hasClass('header-busca-v-center') ) {
		    	$('.header-busca').removeClass('header-busca-v-center');
		  	}else if ( $('.header-busca').hasClass('header-busca-v-bottom') ) {
		    	$('.header-busca').removeClass('header-busca-v-bottom');
		  	}
		});
			//COLOCANDO AS CLASS DE POSICIONAMENTO
			$('.header-busca').addClass(buscavalorAlinhamentoHorizontal);
			$('.header-busca').addClass(buscavalorAlinhamentoVertical);

});
//EDITANDO POSIÇÃO DO CAMPO DE ICPNES
$('#edite-icones').on('click', function(event) {
	event.preventDefault();

	//PEGANDO OS VALORES DOS CAMPOS SELECIONADO PELO USUARIO
	var iconesvalorAlinhamentoHorizontal = $('.modal-edite-icones #select-alinhamento-horizontal').val();
	var iconesvalorAlinhamentoVertical = $('.modal-edite-icones #select-alinhamento-vertical').val();

		//VERIFICANDO SE JÁ EXISTE UM CLASSE DE POSICIONAMENTO HORIZONTAL
		//SE EXISTIR RETIRAR PARA COLOCAR A NOVA ESCOLHIDA PELO USUARIO
		$('header').each(function(index, el) {
			if ( $('.header-icones').hasClass('header-icones-h-left') ) {
		    	$('.header-icones').removeClass('header-icones-h-left');      
		  	}else if ( $('.header-icones').hasClass('header-icones-h-center') ) {
		    	$('.header-icones').removeClass('header-icones-h-center');      
		  	}else if ( $('.header-icones').hasClass('header-icones-h-right') ) {
		    	$('.header-icones').removeClass('header-icones-h-right');
		  	}
		});
		//VERIFICANDO SE JÁ EXISTE UM CLASSE DE POSICIONAMENTO VERTICAL
		//SE EXISTIR RETIRAR PARA COLOCAR A NOVA ESCOLHIDA PELO USUARIO
		$('header').each(function(index, el) {
		  	if ( $('.header-icones').hasClass('header-icones-v-top') ) {
		    	$('.header-icones').removeClass('header-icones-v-top');
		  	}else if ( $('.header-icones').hasClass('header-icones-v-center') ) {
		    	$('.header-icones').removeClass('header-icones-v-center');
		  	}else if ( $('.header-icones').hasClass('header-icones-v-bottom') ) {
		    	$('.header-icones').removeClass('header-icones-v-bottom');
		  	}
		});
			//COLOCANDO AS CLASS DE POSICIONAMENTO
			$('.header-icones').addClass(iconesvalorAlinhamentoHorizontal);
			$('.header-icones').addClass(iconesvalorAlinhamentoVertical);

});
//PEGANDO TODO O CODIGO GERADO PELO USUARIO
$('#salvar-codigo').on('click', function(event) {
	event.preventDefault();
	var code = $('header').html();
	console.log(code);
});